const ELAN_ID = 22078

/**
 * 
 * @param {*} Options 
 */
async function fetchCOINData({
    id = ELAN_ID,
    currency="USD",
    key = "",
}) {

    try {
        const result = await fetch(`https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?id=${id}&convert=${currency}`, {
            method: "GET",
            headers: {
                'X-CMC_PRO_API_KEY': key,
                'Accept': 'application/json',
                'Accept-Encoding': 'deflate, gzip'
            }
        })

        const data = await result.json()
        return data

    } catch (error) {
        return null

    }
}

export {
    ELAN_ID, fetchCOINData
}