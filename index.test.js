import { ELAN_ID, fetchCOINData } from "./coinapi"

let coinmarketapi_mocked_data = {
  status: {},
  data: {}
}

coinmarketapi_mocked_data.data[ELAN_ID] = {
  is_active: 1
}

const key = "b54bcf4d-1bca-4e8e-9a24-22ff2c3d462c"

beforeEach(() => {
  fetch.resetMocks();
});

it("should call the API", async () => {
  fetch.mockResponseOnce(JSON.stringify(coinmarketapi_mocked_data));

  const data = await fetchCOINData({
    key
  })
  console.log(data)
  expect(data).toHaveProperty("data");
  expect(fetch).toHaveBeenCalledTimes(1);
});

it("should return null when error", async () => {
  fetch.mockReject(() => Promise.reject("API is down"));

  const data = await fetchCOINData({
    key
  })

  expect(data).toEqual(null)
  expect(fetch).toHaveBeenCalledWith(
    "https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest?id=22078&convert=CAD", {"headers": {"Accept": "application/json", "Accept-Encoding": "deflate, gzip", "X-CMC_PRO_API_KEY": key}, "method": "GET"}
  )
})