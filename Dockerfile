from node:latest AS build
run apt-get update && apt-get install -y --no-install-recommends dumb-init
workdir /usr/src/app
copy . /usr/src/app
run npm install


from node:18-bullseye-slim
COPY --from=build /usr/bin/dumb-init /usr/bin/dumb-init
user node
workdir /usr/src/app
expose 8080
copy --chown=node:node --from=build /usr/src/app/node_modules /usr/src/app/node_modules
copy --chown=node:node . /usr/src/app
cmd ["dumb-init", "node", "index.js"]