import * as dotenv from 'dotenv'
import discord from 'discord.js'
import http from "http"
import { createLogger, format, transports } from "winston"

import { ELAN_ID, fetchCOINData } from "./coinapi.js"

const logLevels = {
	fatal: 0,
	error: 1,
	warn: 2,
	info: 3,
	debug: 4,
	trace: 5,
};

//loggin server started
const logger = createLogger({
	levels: logLevels,
	format: format.combine(format.timestamp(), format.json()),
	transports: [new transports.Console({
		level: "error"
	})],
});

if (process.env.NODE_ENV !== 'production') {
	logger.add(new transports.Console({
		format: format.simple(),
	}));
}

dotenv.config()
const { Client, GatewayIntentBits, Partials } = discord
const client = new Client({ intents: [] })

async function getPrices() {

	const data = await fetchCOINData({
		ELAN_ID,
		currency: process.env.PREFERRED_CURRENCY,
		key: process.env.COINMARKETCAP_KEY
	})


	if (data == null) {
		logger.error("Unable to fetch api")
		return false
	}

	const currentPrice = data.data[ELAN_ID].quote[process.env.PREFERRED_CURRENCY].price
	const priceChange = data.data[ELAN_ID].quote[process.env.PREFERRED_CURRENCY].percent_change_24h

	const arrow_emoji = priceChange > 0 ? "⬆️" : "🔽"
	
	client.user.setPresence({
		activities: [{
			name: `${(currentPrice).toLocaleString().replace(/,/g, process.env.THOUSAND_SEPARATOR)}${process.env.CURRENCY_SYMBOL} | ${priceChange.toFixed(2)}% ${arrow_emoji}`,
			type: 3 // Use activity type 3 which is "Watching"
		}]
	})


	logger.info(`Updated price to ${currentPrice}`)
}

// Runs when client connects to Discord.
client.on('ready', () => {
	logger.info("Discord bot ready")

	getPrices() // Ping server once on startup
	// Ping the server and set the new status message every x minutes. (Minimum of 1 minute)
	setInterval(getPrices, Math.max(1, process.env.UPDATE_FREQUENCY || 1) * 60 * 1000)
	http.createServer(function (req, res) {
		logger.info(`Request from ${req.rawHeaders}`)
		res.writeHead(200, { 'Content-Type': 'text/plain' });
		res.write('ok');
		res.end();
	}).listen(8080);
})

// Login to Discord
client.login(process.env.DISCORD_TOKEN).then(_ => {
	logger.info("Discord bot has logged in " + client.user.tag)
}).catch(err => {
	logger.fatal("Unable to login to discord " + err)
})